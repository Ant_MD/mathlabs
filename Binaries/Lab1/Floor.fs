#version 330 core

in vec2 fTex;

uniform sampler2D tex1;

out vec4 FragColor;

vec4 gamma(vec4 color) {
	float g = 1 / 2.2;
	return pow(color, vec4(g, g, g, 1.0));
}

void main()
{
    FragColor = gamma(texture(tex1, fTex));
}
