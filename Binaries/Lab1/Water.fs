#version 330 core

uniform sampler2D tTex1;
uniform samplerCube tSky1;

in vec3 vPos;
in vec3 vNorm;
in vec2 vTex;
in vec3 vLightDir;
in vec3 vCameraPos;
in vec3 vViewDir;

out vec4 FragColor;

vec4 fWaterColor = vec4(0.0039, 0.0627, 0.1765, 1.0) ;
vec4 fLightColor = vec4(0.9, 0.9, 0.9, 1.0);

float fDiffuseCoeff = 0.00; 
float fSpecularCoeff = 0.9;
float fAmbientCoeff = 0.8;
float fReflectCoeff = 0.4;

vec4 gamma(vec4 color) {
	float g = 1 / 2.2;
	return pow(color, vec4(g, g, g, 1.0));
}

void main()
{
    vec3 nvNorm = normalize(vNorm);
    vec3 nvLightDir = normalize(vLightDir);
    vec3 nvViewDir = normalize(vViewDir);


    // Diffuse
    float fDiffuse = max(0.0, dot(nvLightDir, nvNorm));

    // Specular
    float fSpecular = pow(max(0.0, dot(nvViewDir, reflect(nvLightDir, nvNorm))), 200.0);

    // Light
    float fLight = fSpecularCoeff * fSpecular + fDiffuseCoeff * fDiffuse;

    // Reflection
    vec3 fReflectDir = reflect(nvViewDir, nvNorm);
    vec4 fReflection = texture(tSky1, fReflectDir);

    // Fresnel
    float fFresnel = min(1.0, 1 - pow(dot(nvViewDir, nvNorm), 0.5));

    // Ambient
    vec3 fRefractView = refract(nvViewDir, vNorm, 1/1.05);

    vec4 fAmbientColor = texture(tSky1, fRefractView);

    float x = vTex.x - 0.5 * fRefractView.x / fRefractView.y * vPos.y;
    float z = vTex.y - 0.5 * fRefractView.z / fRefractView.y * vPos.y;

    if(x > 0.0 && x < 1.0 && z > 0.0 && z < 1.0) {
    	fAmbientColor = texture(tTex1, vec2(x, -z));
    }

    //
    vec4 fAmbient = mix(fAmbientColor, fWaterColor, fAmbientCoeff);

    vec4 fColor = mix(mix(fAmbient, fReflection, fFresnel), fLightColor, fLight);

	FragColor = gamma(fColor);
}
