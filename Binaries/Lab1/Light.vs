#version 330 core

layout (location = 0) in vec4 lPos;
layout (location = 1) in vec4 lNorm;
layout (location = 2) in vec2 lTex;

uniform mat4 mvp;

void main()
{
    gl_Position = lPos * mvp;
}