#version 330 core

layout (location = 0) in vec4 lPos;
layout (location = 1) in vec4 lNorm;
layout (location = 2) in vec2 lTex;

uniform mat4 mvp;
uniform vec3 uLightDir;
uniform vec3 uCameraPos;

out vec3 vPos;
out vec3 vNorm;
out vec2 vTex;
out vec3 vLightDir;
out vec3 vCameraPos;
out vec3 vViewDir;

void main()
{
    gl_Position = lPos * mvp;
    vPos = vec3(lPos);
    vNorm = normalize(vec3(lNorm));
    vTex = lTex;

    vLightDir = uLightDir;
    vCameraPos = uCameraPos;
    vViewDir = lPos.xyz - uCameraPos;
}
