#version 330 core

in vec3 fTex;

uniform samplerCube skybox;

out vec4 FragColor;

vec4 gamma(vec4 color) {
	float g = 1 / 2.2;
	return pow(color, vec4(g, g, g, 1.0));
}

void main()
{
    FragColor = gamma(texture(skybox, fTex));
}
