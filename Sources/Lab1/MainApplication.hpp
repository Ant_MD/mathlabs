#ifndef MATH_LABS_LAB1_MAIN_APPLICATION
#define MATH_LABS_LAB1_MAIN_APPLICATION

#include "../Core/Application.hpp"
#include "../Core/Camera.hpp"
#include "../Core/Object.hpp"
#include "../Core/Primitives/Mesh.hpp"
#include "../Core/Primitives/Shader.hpp"
#include "../Core/Primitives/Texture.hpp"

#include "memory"

class MainApplication : public Core::Application {
public:
  MainApplication();

protected:
  void OnStart() final;
  void OnRedraw() final;
  void OnUpdate() final;

private:
  void GenerateWater();
  void GenerateLight();
  void GenerateFloor();

  void UpdateWater();
  void UpdateCamera();

  const int WIDTH = 100;
  const int HEIGHT = 100;

  std::vector<Core::Vertex> waterVertices_;

  std::shared_ptr<Core::Camera> camera_;

  std::shared_ptr<Core::Object> floor_;
  std::shared_ptr<Core::Object> water_;
  std::shared_ptr<Core::Object> light_;
};

#endif // MATH_LABS_LAB1_MAIN_APPLICATION
