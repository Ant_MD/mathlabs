#include "MainApplication.hpp"

#include "../Core/Math/Math.hpp"
#include "../Core/Primitives/Uniforms/UniformVec3.hpp"

#include "GLFW/glfw3.h"

#include <chrono>
#include <cmath>
#include <iostream>

using namespace Core;
using namespace std;

MainApplication::MainApplication()
    : Application(), camera_(new Camera()), floor_(new Object), water_(new Object()), light_(new Object()) {}

void MainApplication::OnRedraw() {
  ClearColor(0.098f, 0.098f, 0.439f);
  ClearDepth();

  Draw(light_.get(), camera_.get(), {}, false);

  Draw(floor_.get(), camera_.get(), {}, false);

  UniformVec3 camPos("cameraPos", camera_->GetPosition());
  UniformVec3 lightPos("lightPos", light_->GetPosition());
  Draw(water_.get(), camera_.get(), {&camPos, &lightPos}, false);
}

void MainApplication::OnStart() {
  camera_->SetProjection(60.0f, 4.0f / 3.0f, 0.5f, 80.0f);

  GenerateWater();
  GenerateLight();
  GenerateFloor();
}

void MainApplication::OnUpdate() {
  UpdateCamera();

  UpdateWater();
}

void MainApplication::GenerateWater() {
  string waterVsSource = LoadStringFromFile("Binaries/Lab1/Water.vs");
  string waterFsSource = LoadStringFromFile("Binaries/Lab1/Water.fs");

  auto *shader = new Shader();
  shader->Compile(waterVsSource, waterFsSource);
  std::cerr << shader->GetError();

  auto *texture = new Texture();
  texture->LoadFromFile("Binaries/Lab1/Floor.jpg");

  vector<Vertex> data;
  data.reserve(WIDTH * HEIGHT);

  for (int z = 0; z <= HEIGHT; z++) {
    for (int x = 0; x <= WIDTH; x++) {
      Vertex v;
      v.position_.x_ = 2.0f * x / WIDTH - 1.0f;
      v.position_.y_ = 0.0f;
      v.position_.z_ = 2.0f * z / HEIGHT - 1.0f;

      v.texture_.x_ = static_cast<float>(x) / WIDTH;
      v.texture_.y_ = static_cast<float>(z) / WIDTH;

      data.push_back(v);
    }
  }

  vector<unsigned int> indices;
  indices.reserve(WIDTH * HEIGHT * 6);

  for (int z = 0; z < HEIGHT; z++) {
    for (int x = 0; x < WIDTH; x++) {
      int width = WIDTH + 1;
      indices.push_back(static_cast<unsigned int &&>(x + z * width));
      indices.push_back(static_cast<unsigned int &&>(x + (z + 1) * width));
      indices.push_back(static_cast<unsigned int &&>((x + 1) + (z + 1) * width));
      indices.push_back(static_cast<unsigned int &&>(x + z * width));
      indices.push_back(static_cast<unsigned int &&>((x + 1) + (z + 1) * width));
      indices.push_back(static_cast<unsigned int &&>((x + 1) + z * width));
    }
  }

  auto *mesh = new Mesh();
  mesh->SetIndices(indices);
  mesh->SetData(data);

  water_->SetMesh(mesh);
  water_->SetTexture(texture);
  water_->SetShader(shader);

  waterVertices_ = data;
}

void MainApplication::GenerateLight() {
  vector<Vertex> vs = {{{0.0f, -1.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f}},
                       {{-0.5f, 0.0f, 0.5f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f}},
                       {{0.0f, 0.0f, -0.5f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f}},
                       {{0.5f, 0.0f, 0.5f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f}}};

  vector<unsigned int> indices = {1, 2, 3, 0, 2, 1, 0, 3, 2, 0, 1, 3};

  string lightVsSource = LoadStringFromFile("Binaries/Lab1/Light.vs");
  string lightFsSource = LoadStringFromFile("Binaries/Lab1/Light.fs");

  auto *lightShader_ = new Shader();
  lightShader_->Compile(lightVsSource, lightFsSource);

  std::cerr << lightShader_->GetError();

  auto *mesh = new Mesh();

  mesh->SetData(vs);
  mesh->SetIndices(indices);

  light_->SetMesh(mesh);
  light_->SetScale(0.05f);
  light_->SetShader(lightShader_);
  light_->SetPosition({-0.9f, 0.5f, -0.9f});
}

void MainApplication::GenerateFloor() {
  vector<Vertex> vs = {{{-1.0f, 0.0f, 1.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f}},
                       {{-1.0f, 0.0f, -1.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 1.0f}},
                       {{1.0f, 0.0f, -1.0f}, {0.0f, 0.0f, 0.0f}, {1.0f, 1.0f}},
                       {{1.0f, 0.0f, 1.0f}, {0.0f, 0.0f, 0.0f}, {1.0f, 0.0f}}};

  vector<unsigned int> indices = {0, 1, 2, 0, 2, 3};

  auto texture = new Texture();
  texture->LoadFromFile("Binaries/Lab1/Floor.jpg");

  string lightVsSource = LoadStringFromFile("Binaries/Lab1/Floor.vs");
  string lightFsSource = LoadStringFromFile("Binaries/Lab1/Floor.fs");

  auto *shader = new Shader();
  shader->Compile(lightVsSource, lightFsSource);

  std::cerr << shader->GetError();

  auto *mesh = new Mesh();
  mesh->SetData(vs);
  mesh->SetIndices(indices);

  floor_->SetMesh(mesh);
  floor_->SetShader(shader);
  floor_->SetPosition({0.0f, -0.5f, 0.0f});
  floor_->SetTexture(texture);
}

void MainApplication::UpdateWater() {
  auto time = (float)std::clock() / 500;

  for (auto &vertex : waterVertices_) {
    float x = vertex.position_.x_;
    float z = vertex.position_.z_;

    float y = (cos(-time + sqrt(x * x + z * z) * 20)) + 1.0f;

    vertex.position_.y_ = y / 100.0f;
  }

  for (auto z = 1; z < HEIGHT; z++) {
    for (auto x = 1; x < WIDTH; x++) {
      auto width = WIDTH + 1;

      float lh = waterVertices_[(x - 1) + z * width].position_.y_;
      float rh = waterVertices_[(x + 1) + z * width].position_.y_;
      float uh = waterVertices_[x + (z + 1) * width].position_.y_;
      float dh = waterVertices_[x + (z - 1) * width].position_.y_;

      Vec3 a(1.0f / WIDTH, rh - lh, 0.0f);
      Vec3 b(0.0, uh - dh, 1.0f / HEIGHT);

      waterVertices_[x + z * width].normal_ = Vec3::Normalize(Vec3::Cross(b, a));
    }
  }

  water_->GetMesh()->SetData(waterVertices_);
}

void MainApplication::UpdateCamera() {
  Vec3 cameraPos = camera_->GetPosition();
  float yaw = camera_->GetYaw();
  float pitch = camera_->GetPitch();

  if (KeyPressed(GLFW_KEY_D)) {
    cameraPos.x_ += sin(Math::ToRad(yaw + 90)) * 0.05f;
    cameraPos.z_ -= cos(Math::ToRad(yaw + 90)) * 0.05f;
  }

  if (KeyPressed(GLFW_KEY_A)) {
    cameraPos.x_ -= sin(Math::ToRad(yaw + 90)) * 0.05f;
    cameraPos.z_ += cos(Math::ToRad(yaw + 90)) * 0.05f;
  }

  if (KeyPressed(GLFW_KEY_W)) {
    cameraPos.x_ += sin(Math::ToRad(yaw)) * 0.05f * cos(Math::ToRad(pitch));
    cameraPos.y_ -= sin(Math::ToRad(pitch)) * 0.05f;
    cameraPos.z_ -= cos(Math::ToRad(yaw)) * 0.05f * cos(Math::ToRad(pitch));
  }

  if (KeyPressed(GLFW_KEY_S)) {
    cameraPos.x_ -= sin(Math::ToRad(yaw)) * 0.05f * cos(Math::ToRad(pitch));
    cameraPos.y_ += sin(Math::ToRad(pitch)) * 0.05f;
    cameraPos.z_ += cos(Math::ToRad(yaw)) * 0.05f * cos(Math::ToRad(pitch));
  }

  pitch += cursorDelta_.y_;
  camera_->SetPitch(Math::Clamp(-90.f, 90.0f, pitch));

  yaw += cursorDelta_.x_;
  camera_->SetYaw(yaw);

  camera_->SetPosition(cameraPos);
}

int main() {
  std::shared_ptr<Application> app = std::make_shared<MainApplication>();

  app->Start();
}
