#include "Application.hpp"

#include "Camera.hpp"
#include "Object.hpp"
#include "Primitives/Cubemap.hpp"
#include "Primitives/Mesh.hpp"
#include "Primitives/Shader.hpp"
#include "Primitives/Texture.hpp"

#include <fstream>
#include <iostream>
#include <thread>

#include "glad/glad.h"

#include "GLFW/glfw3.h"

using namespace std;
using namespace Core;

const char *WINDOW_TITLE = "Application";
const int WINDOW_WIDTH = 800;
const int WINDOW_HEIGHT = 600;

Application::Application() : cursorDelta_(WINDOW_WIDTH / 2.0f, WINDOW_HEIGHT / 2.0f), envmap_(nullptr) {
  glfwInit();

  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

  window_ = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, WINDOW_TITLE, nullptr, nullptr);

  glfwMakeContextCurrent(window_);

  gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);

  glGenVertexArrays(1, &vao_);
  glBindVertexArray(vao_);

  glfwSetWindowSizeCallback(window_, ResizeWindowHandler);
}

Application::~Application() {
  glDeleteVertexArrays(1, &vao_);

  glfwDestroyWindow(window_);

  glfwTerminate();
}

void Application::Start() {
  OnStart();

  while (!glfwWindowShouldClose(window_)) {
    HandleESC();

    UpdateCursor();

    OnUpdate();

    Redraw();

    glfwPollEvents();
  }
}

void Application::Redraw() {
  glEnable(GL_DEPTH_TEST);

  OnRedraw();

  glfwSwapBuffers(window_);
}

void Application::TrySetUniformMat4(Shader *shader, const std::string &name, Mat4 value) {
  auto location = glGetUniformLocation(shader->GetId(), name.c_str());
  if (location != -1) {
    glUniformMatrix4fv(location, 1, GL_FALSE, static_cast<const GLfloat *>(value.GetData()));
  }
}

void Application::TrySetUniformVec3(Shader *shader, const std::string &name, Vec3 value) {
  auto location = glGetUniformLocation(shader->GetId(), name.c_str());
  if (location != -1) {
    glUniform3f(location, value.x_, value.y_, value.z_);
  }
}

void Application::Draw(Object *object, Camera *camera, const std::vector<Uniform *> &uniforms, bool wired) {
  Mesh *mesh = object->GetMesh();
  Texture *texture = object->GetTexture();
  Shader *shader = object->GetShader();

  mesh->Bind();

  if (texture) {
    glActiveTexture(GL_TEXTURE0);
    texture->Bind();
  }

  if (envmap_) {
    glActiveTexture(GL_TEXTURE1);
    envmap_->Bind();
  }

  shader->Bind();

  for (auto uniform : uniforms) {
    uniform->Bind(shader);
  }

  TrySetUniformMat4(shader, "mvp", camera->GetMat() * object->GetMat());

  if (wired) {
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
  } else {
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  }

  SetupPointers();

  glDrawElements(GL_TRIANGLES, mesh->GetVertexCount(), GL_UNSIGNED_INT, nullptr);
}

void Application::ClearColor(float r, float g, float b) {
  glClearColor(r, g, b, 1.0f);
  glClear(GL_COLOR_BUFFER_BIT);
}

void Application::ClearDepth() { glClear(GL_DEPTH_BUFFER_BIT); }

void Application::SetEnvmap(const std::vector<std::string> &filenames) {
  auto *envmap = new Cubemap();
  envmap->LoadFromFiles(filenames);

  envmap_.reset(envmap);
}

std::string Application::LoadStringFromFile(const std::string &filename) {
  ifstream input(filename);

  input.seekg(0, ios::end);

  string str;
  str.resize(static_cast<unsigned long long int>(input.tellg()));

  input.seekg(0, ios::beg);

  str.assign((std::istreambuf_iterator<char>(input)), std::istreambuf_iterator<char>());

  return str;
}
void Application::ResizeWindowHandler(GLFWwindow * /* window */, int width, int height) {
  glViewport(0, 0, width, height);
}

GLFWwindow *Application::GetWindow() { return window_; }

bool Application::KeyPressed(int key) { return glfwGetKey(window_, key) == GLFW_PRESS; }

void Application::UpdateCursor() {
  int w, h;
  double x, y;

  glfwGetWindowSize(window_, &w, &h);

  glfwGetCursorPos(window_, &x, &y);

  glfwSetCursorPos(window_, (double)w / 2, (double)h / 2);

  cursorDelta_ = Vec2((float)(x - (float)w / 2.0f), (float)(y - (float)h / 2.0f));
}

void Application::HandleESC() {
  if (KeyPressed(GLFW_KEY_ESCAPE)) {
    glfwSetWindowShouldClose(window_, true);
  }
}

void Application::SetupPointers() {
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), nullptr);

  glEnableVertexAttribArray(1);
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void *)(3 * sizeof(float)));

  glEnableVertexAttribArray(2);
  glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void *)(6 * sizeof(float)));
}
