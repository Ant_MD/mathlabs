#ifndef MATH_LABS_CORE_CAMERA_HPP
#define MATH_LABS_CORE_CAMERA_HPP

#include "Math/Mat4.hpp"
#include "Math/Vec3.hpp"

namespace Core {

class Camera {
public:
  Camera();

  void SetProjection(float fov, float aspect, float near, float far);

  void SetPosition(const Vec3 &position);
  Vec3 GetPosition();

  void SetYaw(float value);
  float GetYaw();

  void SetPitch(float value);
  float GetPitch();

  Mat4 GetMat();
  Mat4 GetView();

private:
  float yaw_;
  float pitch_;

  float fov_;
  float aspect_;
  float near_;
  float far_;

  Core::Vec3 pos_;
};

} // namespace Core

#endif // MATH_LABS_CORE_CAMERA_HPP
