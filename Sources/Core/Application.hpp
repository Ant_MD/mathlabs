#ifndef MATH_LABS_CORE_APPLICATION
#define MATH_LABS_CORE_APPLICATION

#include "Math/Mat4.hpp"
#include "Math/Vec2.hpp"
#include "Primitives/Uniforms/Uniform.hpp"

#include <memory>
#include <string>
#include <vector>

class GLFWwindow;

namespace Core {

class Mesh;
class Camera;
class Object;
class Texture;
class Shader;
class Cubemap;

class Application {
public:
  Application();
  virtual ~Application();

  Application(const Application &) = delete;
  Application &operator=(const Application &) = delete;

  static std::string LoadStringFromFile(const std::string &filename);

  void Start();

protected:
  virtual void OnStart(){};
  virtual void OnUpdate(){};
  virtual void OnRedraw(){};

  void Draw(Object *object, Camera *camera, const std::vector<Uniform *> &uniforms, bool wired = false);

  void ClearColor(float r, float g, float b);
  void ClearDepth();

  void SetEnvmap(const std::vector<std::string> &filenames);

  GLFWwindow *GetWindow();

  bool KeyPressed(int key);

  Core::Vec2 cursorDelta_;

private:
  void Redraw();
  void UpdateCursor();
  void HandleESC();
  void SetupPointers();

  void TrySetUniformMat4(Shader *, const std::string &name, Mat4 value);
  void TrySetUniformVec3(Shader *, const std::string &name, Vec3 value);

  static void ResizeWindowHandler(GLFWwindow *window, int width, int height);

  GLFWwindow *window_;

  unsigned int vao_;

  std::shared_ptr<Cubemap> envmap_;
};
} // namespace Core

#endif // MATH_LABS_CORE_APPLICATION
