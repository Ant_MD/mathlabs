#include "Object.hpp"

#include "Primitives/Mesh.hpp"
#include "Primitives/Shader.hpp"
#include "Primitives/Texture.hpp"

using namespace Core;

Core::Object::Object() : scale_(1.0f) {}

Shader *Object::GetShader() { return shader0_.get(); }

void Object::SetShader(Shader *shader) { shader0_.reset(shader); }

Mesh *Core::Object::GetMesh() { return mesh0_.get(); }

void Core::Object::SetMesh(Mesh *mesh) { mesh0_.reset(mesh); }

Texture *Core::Object::GetTexture() { return texture0_.get(); }

void Core::Object::SetTexture(Texture *texture) { texture0_.reset(texture); }

Core::Vec3 Core::Object::GetPosition() { return position_; }

void Core::Object::SetPosition(const Core::Vec3 &value) { position_ = value; }

float Core::Object::GetScale() { return scale_; }

void Core::Object::SetScale(float scale) { scale_ = scale; }

Core::Mat4 Core::Object::GetMat() {
  Mat4 tran = Mat4::Translation(Vec3(position_.x_, position_.y_, position_.z_));
  // Mat4 rot = Mat4::Rotation();
  Mat4 scale = Mat4::Scale(scale_, scale_, scale_);
  return tran * scale;
}
