#include "Camera.hpp"

#include "Math/Math.hpp"

#include <cmath>

using namespace Core;

Core::Camera::Camera() : yaw_(0.0f), pitch_(0.0f), fov_(60.0f), aspect_(1.0f), near_(0.5f), far_(80.0f) {}

void Core::Camera::SetProjection(float fov, float aspect, float near, float far) {
  fov_ = fov;
  aspect_ = aspect;
  near_ = near;
  far_ = far;
}

void Core::Camera::SetPosition(const Core::Vec3 &position) { pos_ = position; }

Core::Vec3 Core::Camera::GetPosition() { return pos_; }

void Core::Camera::SetYaw(float value) { yaw_ = value; }

float Core::Camera::GetYaw() { return yaw_; }

void Core::Camera::SetPitch(float value) { pitch_ = value; }

float Core::Camera::GetPitch() { return pitch_; }

Core::Mat4 Core::Camera::GetMat() {
  Mat4 pos = Mat4::Translation({-pos_.x_, -pos_.y_, -pos_.z_});
  Mat4 proj = Mat4::Projection(fov_, aspect_, near_, far_);
  Mat4 rot =
      Mat4::Rotation({1.0f, 0.0f, 0.0f}, Math::ToRad(pitch_)) * Mat4::Rotation({0.0f, 1.0f, 0.0f}, Math::ToRad(yaw_));

  return proj * rot * pos;
}

Mat4 Camera::GetView() {
  Mat4 pos = Mat4::Translation({-pos_.x_, -pos_.y_, pos_.z_});
  Mat4 rot =
      Mat4::Rotation({1.0f, 0.0f, 0.0f}, Math::ToRad(pitch_)) * Mat4::Rotation({0.0f, 1.0f, 0.0f}, Math::ToRad(yaw_));

  return rot * pos;
}
