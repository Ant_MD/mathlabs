#ifndef MATH_LABS_CORE_MESH_HPP
#define MATH_LABS_CORE_MESH_HPP

#include "../Math/Vec2.hpp"
#include "../Math/Vec3.hpp"

#include <vector>


namespace Core {

struct Vertex {
  Vec3 position_;
  Vec3 normal_;
  Vec2 texture_;
};

class Mesh {
public:
  Mesh();
  ~Mesh();

  void Bind();

  void SetData(const std::vector<Vertex> &data);
  void SetIndices(const std::vector<unsigned int> &indices);

  int GetVertexCount();
  unsigned int GetVbo();
  unsigned int GetEbo();

private:
  unsigned int vbo_;
  unsigned int ebo_;
  int vertexCount_;
};
} // namespace Core

#endif // MATH_LABS_CORE_MESH_HPP
