#ifndef MATH_LABS_CORE_SHADER_HPP
#define MATH_LABS_CORE_SHADER_HPP

#include "../Math/Mat4.hpp"

#include <memory>
#include <string>

namespace Core {

class Shader {
public:
  Shader();
  ~Shader();

  unsigned int GetId();

  void Bind();
  void Compile(const std::string &vsSource, const std::string &fsSource);

  std::string GetError();

  Shader(const Shader &) = delete;
  Shader &operator=(const Shader &) = delete;

private:
  unsigned int CreateShader(const std::string &name, const std::string &source, unsigned int type);

  std::string errorMsg_;

  unsigned int id_;
};
} // namespace Core

#endif // MATH_LABS_CORE_SHADER_HPP
