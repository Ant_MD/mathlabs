#ifndef MATH_LABS_CUBEMAP_HPP
#define MATH_LABS_CUBEMAP_HPP

#include <string>
#include <vector>

namespace Core {
class Cubemap {
public:
  Cubemap();
  ~Cubemap();

  void Bind();
  void LoadFromFiles(const std::vector<std::string> &filenames);

private:
  unsigned int id_;
};
} // namespace Core

#endif // MATH_LABS_CUBEMAP_HPP
