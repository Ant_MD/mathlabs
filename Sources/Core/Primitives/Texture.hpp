#ifndef MATH_LABS_CORE_TEXTURE_HPP
#define MATH_LABS_CORE_TEXTURE_HPP

#include <string>

namespace Core {

class Texture {
public:
  Texture();
  ~Texture();

  void Bind();
  void LoadFromFile(const std::string &filename);

private:
  unsigned int id_;
};
} // namespace Core

#endif // MATH_LABS_CORE_TEXTURE_HPP
