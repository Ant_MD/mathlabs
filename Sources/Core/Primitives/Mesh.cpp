#include "Mesh.hpp"

#include "glad/glad.h"

using namespace std;
using namespace Core;

Mesh::Mesh() : vbo_(0), ebo_(0), vertexCount_(0) {}

Mesh::~Mesh() {
  if (vbo_) {
    glDeleteBuffers(1, &vbo_);
  }
  if (ebo_) {
    glDeleteBuffers(1, &ebo_);
  }
}

void Mesh::Bind() {
  glBindBuffer(GL_ARRAY_BUFFER, vbo_);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo_);
}

void Mesh::SetData(const std::vector<Vertex> &data) {
  if (!vbo_) {
    glGenBuffers(1, &vbo_);
  }

  glBindBuffer(GL_ARRAY_BUFFER, vbo_);

  glBufferData(GL_ARRAY_BUFFER, data.size() * sizeof(Vertex), data.data(), GL_STATIC_DRAW);

  glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void Mesh::SetIndices(const std::vector<unsigned int> &indices) {
  if (!ebo_) {
    glGenBuffers(1, &ebo_);
  }

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo_);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), indices.data(), GL_STATIC_DRAW);

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

  vertexCount_ = static_cast<int>(indices.size());
}

int Mesh::GetVertexCount() { return vertexCount_; }

unsigned int Mesh::GetEbo() { return ebo_; }
unsigned int Mesh::GetVbo() { return vbo_; }
