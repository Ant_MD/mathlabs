#include "Shader.hpp"

#include "glad/glad.h"

#include "iostream"

using namespace std;
using namespace Core;

Shader::Shader() : id_(0) {}

Shader::~Shader() { glDeleteProgram(id_); }

unsigned int Shader::GetId() { return id_; }

void Shader::Bind() { glUseProgram(id_); }

void Shader::Compile(const string &vsSource, const string &fsSource) {
  if (id_) {
    glDeleteProgram(id_);
    id_ = 0;
  }

  errorMsg_ = "";

  unsigned int vertexShader = CreateShader("VERTEX SHADER", vsSource, GL_VERTEX_SHADER);
  unsigned int fragmentShader = CreateShader("FRAGMENT SHADER", fsSource, GL_FRAGMENT_SHADER);

  int success;

  id_ = glCreateProgram();
  glAttachShader(id_, vertexShader);
  glAttachShader(id_, fragmentShader);
  glLinkProgram(id_);

  glGetProgramiv(id_, GL_LINK_STATUS, &success);
  if (!success) {
    char buff[512];
    glGetProgramInfoLog(id_, 512, nullptr, buff);
    errorMsg_ = errorMsg_ + "LINKING\n" + buff;
  }

  glDeleteShader(fragmentShader);
  glDeleteShader(vertexShader);
}

std::string Shader::GetError() { return errorMsg_; }

unsigned int Shader::CreateShader(const string &name, const string &source, unsigned int type) {
  unsigned int shader = glCreateShader(type);

  const char *source_c = source.c_str();

  glShaderSource(shader, 1, &source_c, nullptr);
  glCompileShader(shader);

  int success;

  glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
  if (!success) {
    char buff[512];

    glGetShaderInfoLog(shader, 512, nullptr, buff);

    errorMsg_ = errorMsg_ + name + "\n" + buff;
  }

  return shader;
}
