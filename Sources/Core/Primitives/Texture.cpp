#include "Texture.hpp"

#include "glad/glad.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

using namespace std;
using namespace Core;

Texture::Texture() : id_(0) {}

Texture::~Texture() {
  if (id_) {
    glDeleteTextures(1, &id_);
  }
}

void Texture::LoadFromFile(const string &filename) {
  if (id_) {
    glDeleteTextures(1, &id_);
  }

  glGenTextures(1, &id_);

  glBindTexture(GL_TEXTURE_2D, id_);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

  int width, height, channels;
  stbi_set_flip_vertically_on_load(true);

  unsigned char *data = stbi_load(filename.c_str(), &width, &height, &channels, 0);

  glTexImage2D(GL_TEXTURE_2D, 0, GL_SRGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
  glGenerateMipmap(GL_TEXTURE_2D);

  stbi_image_free(data);
}

void Texture::Bind() { glBindTexture(GL_TEXTURE_2D, id_); }
