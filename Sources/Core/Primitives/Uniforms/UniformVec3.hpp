#ifndef MATH_LABS_UNIFORM_VEC3_HPP
#define MATH_LABS_UNIFORM_VEC3_HPP

#include "Uniform.hpp"
#include "../../Math/Vec3.hpp"

#include <iostream>
#include <utility>

namespace Core {

class Shader;

class UniformVec3 : public Uniform {
public:
  UniformVec3(std::string name, Vec3 value) : name_(std::move(name)), value_(value) {}

  void Bind(Shader *shader) final;

private:
  std::string name_;
  Vec3 value_;
};

} // namespace Core

#endif // MATH_LABS_UNIFORM_VEC3_HPP
