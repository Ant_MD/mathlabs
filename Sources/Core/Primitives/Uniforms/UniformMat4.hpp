#ifndef MATH_LABS_UNIFORM_MAT4_HPP
#define MATH_LABS_UNIFORM_MAT4_HPP

#include "../../Math/Mat4.hpp"
#include "Uniform.hpp"

#include <iostream>

namespace Core {

class Shader;

class UniformMat4 : public Uniform {
public:
  UniformMat4(std::string name, const Mat4 &value): name_(std::move(name)), value_(value) {}

  void Bind(Shader *shader) final;

private:
  std::string name_;
  Mat4 value_;
};
} // namespace Core

#endif // MATH_LABS_UNIFORM_MAT4_HPP
