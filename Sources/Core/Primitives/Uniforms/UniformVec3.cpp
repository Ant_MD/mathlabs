#include "UniformVec3.hpp"

#include "../Shader.hpp"

#include "glad/glad.h"

void Core::UniformVec3::Bind(Core::Shader *shader) {
  auto location = glGetUniformLocation(shader->GetId(), name_.c_str());
  glUniform3f(location, value_.x_, value_.y_, value_.z_);
}
