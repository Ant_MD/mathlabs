#include "UniformMat4.hpp"

#include "../Shader.hpp"

#include "glad/glad.h"

void Core::UniformMat4::Bind(Core::Shader *shader) {
  auto location = glGetUniformLocation(shader->GetId(), name_.c_str());
  glUniformMatrix4fv(location, 1, GL_FALSE, static_cast<const GLfloat *>(value_.GetData()));
}
