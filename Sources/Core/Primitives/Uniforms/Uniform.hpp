#ifndef MATH_LABS_UNIFORM_HPP
#define MATH_LABS_UNIFORM_HPP

namespace Core {

class Shader;

class Uniform {
public:
  virtual ~Uniform() = default;

  virtual void Bind(Shader *shader) = 0;
};

} // namespace Core

#endif // MATH_LABS_UNIFORM_HPP
