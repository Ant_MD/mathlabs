#include "Cubemap.hpp"

#include "glad/glad.h"

#include "stb_image.h"

using namespace Core;

Cubemap::Cubemap() : id_(0) {}

Cubemap::~Cubemap() {
  if (id_) {
    glDeleteTextures(1, &id_);
  }
}

void Cubemap::Bind() { glBindTexture(GL_TEXTURE_CUBE_MAP, id_); }

void Cubemap::LoadFromFiles(const std::vector<std::string> &filenames) {
  stbi_set_flip_vertically_on_load(false);

  for (unsigned int i = 0; i < filenames.size(); i++) {
    int width, height, channels;

    unsigned char *data = stbi_load(filenames[i].c_str(), &width, &height, &channels, 0);

    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_SRGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
    glGenerateMipmap(GL_TEXTURE_2D);

    stbi_image_free(data);
  }

  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
}
