#ifndef MATH_LABS_CORE_OBJECT_HPP
#define MATH_LABS_CORE_OBJECT_HPP

#include "Math/Mat4.hpp"
#include "Math/Vec3.hpp"

#include <memory>

namespace Core {

class Shader;
class Mesh;
class Texture;

class Object {
public:
  Object();

  Shader *GetShader();
  void SetShader(Shader *shader);

  Mesh *GetMesh();
  void SetMesh(Mesh *mesh);

  Texture *GetTexture();
  void SetTexture(Texture *texture);

  Vec3 GetPosition();
  void SetPosition(const Vec3 &value);

  float GetScale();
  void SetScale(float scale);

  Mat4 GetMat();

private:
  std::shared_ptr<Shader> shader0_;
  std::shared_ptr<Mesh> mesh0_;
  std::shared_ptr<Texture> texture0_;

  Vec3 position_;

  float scale_;
};
} // namespace Core

#endif // MATH_LABS_CORE_OBJECT_HPP
