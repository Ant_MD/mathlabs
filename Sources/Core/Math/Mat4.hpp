#ifndef MATH_LABS_CORE__MAT4_HPP
#define MATH_LABS_CORE__MAT4_HPP

#include "Vec3.hpp"

namespace Core {

class Mat4 {
public:
  Mat4();
  Mat4(float v00, float v10, float v20, float v30, float v01, float v11, float v21, float v31, float v02, float v12,
       float v22, float v32, float v03, float v13, float v23, float v33);

  Mat4 operator*(const Mat4 &a);

  float Get(int x, int y) const;
  void Set(int x, int y, float value);

  void *GetData();

  static Mat4 Identity();

  static Mat4 Translation(const Core::Vec3 &direction);
  static Mat4 Rotation(const Core::Vec3 &axis, float angle);
  static Mat4 Projection(float fov, float aspect, float near, float far);
  static Mat4 Scale(float x, float y, float z);

private:
  float v_[16];
};
} // namespace Core

#endif // MATH_LABS_CORE__MAT4_HPP
