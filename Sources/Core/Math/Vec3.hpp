#ifndef MATH_LABS_CORE_VEC3_HPP
#define MATH_LABS_CORE_VEC3_HPP

namespace Core {

class Vec3 {
public:
  Vec3() : x_(0.0f), y_(0.0f), z_(0.0f) {}
  Vec3(float x, float y, float z) : x_(x), y_(y), z_(z) {}

  static Vec3 Cross(const Vec3 &a, const Vec3 &b);
  static Vec3 Normalize(const Vec3 &a);

  float x_;
  float y_;
  float z_;
};
} // namespace Core
#endif // MATH_LABS_CORE_VEC3_HPP
