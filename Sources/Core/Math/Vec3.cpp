#include "Vec3.hpp"

#include "cmath"

using namespace Core;

Vec3 Vec3::Cross(const Vec3 &a, const Vec3 &b) {
  return {a.y_ * b.z_ - b.y_ * a.z_, a.z_ * b.x_ - b.z_ * a.x_, a.x_ * b.y_ - b.x_ * a.y_};
}

Vec3 Vec3::Normalize(const Vec3 &a) {
  float len = sqrtf(a.x_ * a.x_ + a.y_ * a.y_ + a.z_ * a.z_);

  return {a.x_ / len, a.y_ / len, a.z_ / len};
}
