#ifndef MATH_LABS_CORE_VEC2_HPP
#define MATH_LABS_CORE_VEC2_HPP

namespace Core {

class Vec2 {
public:
  Vec2() : x_(0.0f), y_(0.0f) {}
  Vec2(float x, float y) : x_(x), y_(y) {}

  float x_;
  float y_;
};

} // namespace Core

#endif // MATH_LABS_CORE_VEC2_HPP
