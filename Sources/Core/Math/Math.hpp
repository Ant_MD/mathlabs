#ifndef MATH_LABS_MATH_HPP
#define MATH_LABS_MATH_HPP

namespace Core {

class Math {
public:
  static float Clamp(float min, float max, float value);

  static float ToGrad(float rad);

  static float ToRad(float grad);

  static float Pi();

  static float Length(float x1, float y1);

  static float Sqr(float x);
};

} // namespace Core

#endif // MATH_LABS_MATH_HPP
