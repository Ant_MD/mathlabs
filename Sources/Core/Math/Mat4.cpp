#include "Mat4.hpp"

#include "Math.hpp"

#include <cmath>

using namespace std;
using namespace Core;

Mat4::Mat4() : v_{0.0f} {}

Mat4::Mat4(float v00, float v10, float v20, float v30, float v01, float v11, float v21, float v31, float v02, float v12,
           float v22, float v32, float v03, float v13, float v23, float v33)
    : v_{
          v00, v10, v20, v30, v01, v11, v21, v31, v02, v12, v22, v32, v03, v13, v23, v33,
      } {}

Mat4 Mat4::operator*(const Mat4 &a) {
  Mat4 temp;

  for (auto i = 0; i < 4; i++) {
    for (auto j = 0; j < 4; j++) {
      for (auto k = 0; k < 4; k++) {
        float value = temp.Get(i, j) + a.Get(i, k) * Get(k, j);
        temp.Set(i, j, value);
      }
    }
  }

  return temp;
}

float Mat4::Get(int x, int y) const { return v_[x + y * 4]; }

void Mat4::Set(int x, int y, float value) { v_[x + y * 4] = value; }

void *Mat4::GetData() { return v_; }

Mat4 Mat4::Identity() {
  return {1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f};
}

Mat4 Mat4::Translation(const Core::Vec3 &direction) {
  return {1.0f, 0.0f, 0.0f, direction.x_, 0.0f, 1.0f, 0.0f, direction.y_,
          0.0f, 0.0f, 1.0f, direction.z_, 0.0f, 0.0f, 0.0f, 1.0f};
}

Mat4 Mat4::Rotation(const Core::Vec3 &axis, float angle) {
  float cosA = cos(angle);
  float sinA = sin(angle);
  float mCosA = 1.0f - cosA;

  float x = axis.x_;
  float y = axis.y_;
  float z = axis.z_;

  return {cosA + x * x * mCosA,
          x * y * mCosA - z * sinA,
          x * z * mCosA + y * sinA,
          0.0f,
          y * x * mCosA + z * sinA,
          cosA + y * y * mCosA,
          y * z * mCosA - x * sinA,
          0.0f,
          z * x * mCosA - y * sinA,
          z * y * mCosA + x * sinA,
          cosA + z * z * mCosA,
          0.0f,
          0.0f,
          0.0f,
          0.0f,
          1.0f};
}

Mat4 Mat4::Projection(float fov, float aspect, float near, float far) {
  auto tanFov = static_cast<float>(tan(fov * 0.5f / 180.0f * Math::Pi()));

  return {1.0f / (aspect * tanFov),
          0.0f,
          0.0f,
          0.0f,
          0.0f,
          1.0f / tanFov,
          0.0f,
          0.0f,
          0.0f,
          0.0f,
          (near + far) / (near - far),
          -1.0f,
          0.0f,
          0.0f,
          2.0f * near * far / (near - far),
          0.0f};
}

Mat4 Mat4::Scale(float x, float y, float z) {
  return {x, 0.0f, 0.0f, 0.0f, 0.0f, y, 0.0f, 0.0f, 0.0f, 0.0f, z, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f};
}
