#include "Math.hpp"

#include <cmath>

using namespace Core;

float Core::Math::Clamp(float min, float max, float value) {
  if (value < min) {
    return min;
  } else if (value > max) {
    return max;
  } else {
    return value;
  }
}

float Math::ToGrad(float grad) { return static_cast<float>(grad / M_PI * 180.0f); }

float Math::ToRad(float rad) { return static_cast<float>(rad * M_PI / 180.0f); }

float Math::Pi() { return static_cast<float>(M_PI); }

float Math::Length(float x1, float y1) { return std::sqrt(Sqr(x1) + Sqr(y1)); }

float Math::Sqr(float x) { return x * x; }
