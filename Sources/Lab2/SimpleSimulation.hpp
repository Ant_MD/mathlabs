#ifndef MATH_LABS_LAB2_SIMPLE_SIMULATION_HPP
#define MATH_LABS_LAB2_SIMPLE_SIMULATION_HPP

#include "../Core/Primitives/Mesh.hpp"
#include "WaterSimulation.hpp"

#include <vector>

class SimpleSimulation : public WaterSimulation {
public:
  SimpleSimulation(int w, int h);

  Water &GetWater() { return water_; };

  void Update() final;

private:
  int w_;
  int h_;

  float time_;

  Water water_;
};

#endif // MATH_LABS_LAB2_SIMPLE_SIMULATION_HPP
