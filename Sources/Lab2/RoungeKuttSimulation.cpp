#include "RoungeKuttSimulation.hpp"

#include "../Core/Math/Math.hpp"

#include <cmath>

using namespace Core;
using namespace std;

RoungeKuttSimulation::RoungeKuttSimulation(int w, int h, const Water &defaultWater)
    : w_(w), h_(h), time_(0.0f), vp_(1.0f), step_(2.0f / (w_ - 1)), deltatime_(0.12f * (step_ / vp_)),
      water(defaultWater), newWater_(defaultWater) {}

WaterParams RoungeKuttSimulation::WaveFunction(const WaterParams &v, int i, int j) {
  WaterParams vector;
  vector.h_ = v.speed_;

  float left = water.GetHeight(w_ - 1, j);
  float right = water.GetHeight(0, j);
  float up = water.GetHeight(i, 0);
  float down = water.GetHeight(i, h_ - 1);

  vector.speed_ = ((vp_ * vp_) / (step_ * step_)) *
                  (water.TryGetHeight(i + 1, j, right) + water.TryGetHeight(i - 1, j, left) +
                   water.TryGetHeight(i, j + 1, up) + water.TryGetHeight(i, j - 1, down) - 4.0f * v.h_);
  return vector;
}

void RoungeKuttSimulation::Update() {
  Water waterBuffer = water;
  Water k1(w_, h_);
  Water k2(w_, h_);
  Water k3(w_, h_);
  WaterParams buffer;

  for (int i = 0; i < (w_ - 0); i++) {
    for (int j = 0; j < (h_ - 0); j++) {
      buffer.h_ = water.GetHeight(i, j);
      buffer.speed_ = water.GetSpeed(i, j);
      buffer = WaveFunction(buffer, i, j);
      k1.SetHeight(i, j, buffer.h_);
      k1.SetSpeed(i, j, buffer.speed_);

      buffer.h_ = water.GetHeight(i, j) + k1.GetHeight(i, j) * deltatime_ / 2.0f;
      buffer.speed_ = water.GetSpeed(i, j) + k1.GetSpeed(i, j) * deltatime_ / 2.0f;

      waterBuffer.SetHeight(i, j, buffer.h_);
      waterBuffer.SetSpeed(i, j, buffer.speed_);
    }
  }

  for (int i = 0; i < (w_ - 0); i++) {
    for (int j = 0; j < (h_ - 0); j++) {
      buffer.h_ = waterBuffer.GetHeight(i, j);
      buffer.speed_ = waterBuffer.GetSpeed(i, j);
      buffer = WaveFunction(buffer, i, j);
      k2.SetHeight(i, j, buffer.h_);
      k2.SetSpeed(i, j, buffer.speed_);

      buffer.h_ = water.GetHeight(i, j) + k2.GetHeight(i, j) * deltatime_ / 2.0f;
      buffer.speed_ = water.GetSpeed(i, j) + k2.GetSpeed(i, j) * deltatime_ / 2.0f;

      waterBuffer.SetHeight(i, j, buffer.h_);
      waterBuffer.SetSpeed(i, j, buffer.speed_);
    }
  }

  for (int i = 0; i < (w_ - 0); i++) {
    for (int j = 0; j < (h_ - 0); j++) {
      buffer.h_ = waterBuffer.GetHeight(i, j);
      buffer.speed_ = waterBuffer.GetSpeed(i, j);
      buffer = WaveFunction(buffer, i, j);
      k3.SetHeight(i, j, buffer.h_);
      k3.SetSpeed(i, j, buffer.speed_);

      buffer.h_ = water.GetHeight(i, j) + k3.GetHeight(i, j) * deltatime_;
      buffer.speed_ = water.GetSpeed(i, j) + k3.GetSpeed(i, j) * deltatime_;

      waterBuffer.SetHeight(i, j, buffer.h_);
      waterBuffer.SetSpeed(i, j, buffer.speed_);
    }
  }

  for (int i = 0; i < (w_ - 0); i++) {
    for (int j = 0; j < (h_ - 0); j++) {
      buffer.h_ = waterBuffer.GetHeight(i, j);
      buffer.speed_ = waterBuffer.GetSpeed(i, j);
      buffer = WaveFunction(buffer, i, j);

      buffer.h_ = water.GetHeight(i, j) + (deltatime_ / 6.0f *
                                           (k1.GetHeight(i, j) + (deltatime_ / 2.0f) * k2.GetHeight(i, j) +
                                            (deltatime_ / 2.0f) * k3.GetHeight(i, j) + buffer.h_));
      buffer.speed_ = water.GetSpeed(i, j) + (deltatime_ / 6.0f *
                                              (k1.GetSpeed(i, j) + (deltatime_ / 2.0f) * k2.GetSpeed(i, j) +
                                               (deltatime_ / 2.0f) * k3.GetSpeed(i, j) + buffer.speed_));

      waterBuffer.SetHeight(i, j, buffer.h_);
      waterBuffer.SetSpeed(i, j, buffer.speed_);
    }
  }

  water = waterBuffer;
}
