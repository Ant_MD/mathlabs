#ifndef MATH_LABS_LAB2_WATER_SIMULATION_HPP
#define MATH_LABS_LAB2_WATER_SIMULATION_HPP

#include "Water.hpp"

#include <vector>

class WaterSimulation {
public:
  virtual ~WaterSimulation() = default;

  virtual Water &GetWater() = 0;

  virtual void Update(){};
};

#endif // MATH_LABS_LAB2_WATER_SIMULATION_HPP
