#ifndef MATH_LABS_LAB2_FINITE_DIFFERENCE_SIMULATION_HPP
#define MATH_LABS_LAB2_FINITE_DIFFERENCE_SIMULATION_HPP

#include "../Core/Primitives/Mesh.hpp"
#include "Water.hpp"
#include "WaterSimulation.hpp"

#include <vector>

class FiniteDifferenceSimulation : public WaterSimulation {
public:
  FiniteDifferenceSimulation(int w, int h, const Water &defaultWater);

  Water &GetWater() final { return water; };

  void Update() final;

private:
  WaterParams WaveFunction(const WaterParams &wp, int i, int j);

  int w_;
  int h_;

  float time_;
  float vp_;
  float step_;
  float deltatime_;

  Water water;
  Water newWater_;
};

#endif // MATH_LABS_LAB2_FINITE_DIFFERENCE_SIMULATION_HPP
