#include "FiniteDifferenceSimulation.hpp"

#include <cmath>

using namespace Core;
using namespace std;

WaterParams FiniteDifferenceSimulation::WaveFunction(const WaterParams &v, int i, int j) {
  WaterParams vector;
  vector.h_ = v.speed_;

  float left = water.GetHeight(w_ - 1, j);
  float right = water.GetHeight(0, j);
  float up = water.GetHeight(i, 0);
  float down = water.GetHeight(i, h_ - 1);

  vector.speed_ = ((vp_ * vp_) / (step_ * step_)) *
                  (water.TryGetHeight(i + 1, j, right) + water.TryGetHeight(i - 1, j, left) +
                   water.TryGetHeight(i, j + 1, up) + water.TryGetHeight(i, j - 1, down) - 4.0f * v.h_);
  return vector;
}

FiniteDifferenceSimulation::FiniteDifferenceSimulation(int w, int h, const Water &defaultWater)
    : w_(w), h_(h), time_(0.0f), vp_(1.0f), step_(1.0f / (w_ + 1)), deltatime_(0.015f * (step_ / vp_)),
      water(defaultWater), newWater_(defaultWater) {}

void FiniteDifferenceSimulation::Update() {
  for (int i = 0; i < (w_ - 0); i++) {
    for (int j = 0; j < (h_ - 0); j++) {
      WaterParams vector;
      WaterParams futureVector;
      WaterParams newVector;
      WaterParams result;

      vector.h_ = water.GetHeight(i, j);
      vector.speed_ = water.GetSpeed(i, j);
      futureVector = WaveFunction(vector, i, j);

      newVector.h_ = vector.h_ + futureVector.h_ * deltatime_;
      newVector.speed_ = vector.speed_ + futureVector.speed_ * deltatime_;
      newVector = WaveFunction(newVector, i, j);

      result.h_ = vector.h_ + deltatime_ * ((newVector.h_ + futureVector.h_) / 2.0f);
      result.speed_ = vector.speed_ + deltatime_ * ((newVector.speed_ + futureVector.speed_) / 2.0f);

      newWater_.SetHeight(i, j, result.h_);
      newWater_.SetSpeed(i, j, result.speed_);
    }
  }
  water = newWater_;
}
