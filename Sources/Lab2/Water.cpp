#include "Water.hpp"

#include "../Core/Math/Math.hpp"

#include <cmath>

using namespace std;
using namespace Core;

Water::Water(int w, int h) : height_(w * h, 0.0f), speed_(w * h, 0.0f), energy_(w * h, 0.0f), w_(w), h_(h) {
  data_.reserve(w_ * h_);

  for (int z = 0; z < h_; z++) {
    for (int x = 0; x < w_; x++) {
      Vertex v;
      v.position_.x_ = 2.0f * x / w_ - 1.0f;
      v.position_.y_ = 2.0f;
      v.position_.z_ = 2.0f * z / h_ - 1.0f;

      v.texture_.x_ = static_cast<float>(x) / w_;
      v.texture_.y_ = static_cast<float>(z) / w_;

      data_.push_back(v);
    }
  }
}

float Water::GetHeight(int x, int y) {
  // return data_[x + y * w_].position_.y_;
  return height_[x + y * w_];
}

float Water::TryGetHeight(int x, int y, float def) {
  if (x >= 0 && x < w_ && y >= 0 && y < h_) {
    // return data_[x + y * w_].position_.y_;
    return height_[x + y * w_];
  } else {
    return def;
  }
}

void Water::SetHeight(int x, int y, float value) {
  // data_[x + y * w_].position_.y_ = value;
  height_[x + y * w_] = value;
}

float Water::GetSpeed(int x, int y) { return speed_[x + y * w_]; }

void Water::SetSpeed(int x, int y, float value) { speed_[x + y * w_] = value; }

Water Water::FlatWater(int w, int h, float height) {
  auto water = Water(w, h);

  for (int y = 0; y < h; y++) {
    for (int x = 0; x < w; x++) {
      water.SetHeight(x, y, height);
    }
  }

  return water;
}

Water Water::HillWater(int w, int h, float r, float a, float height) {
  auto water = Water(w, h);

  for (int y = 0; y < h; y++) {
    for (int x = 0; x < w; x++) {
      float len = Math::Length(w / 2.0f - x, h / 2.0f - y);
      float value = height;

      if (len / (r / 2.0f) < 1.0f) {
        value += a / 2.0f * (cos(Math::Pi() * len / r) + 1);
      }

      water.SetHeight(x, y, value);
    }
  }

  return water;
}

std::vector<Core::Vertex> &Water::GetData() {
  for (int i = 0; i < data_.size(); i++) {
    data_[i].position_.y_ = height_[i];
  }

  for (int z = 0; z < h_; z++) {
    for (int x = 0; x < w_; x++) {
      auto width = w_;

      float ch = data_[x + z * width].position_.y_;

      float lh = ch;
      float rh = ch;
      float uh = ch;
      float dh = ch;

      if (x > 0) {
        lh = data_[(x - 1) + z * width].position_.y_;
      }

      if (x < w_ - 1) {
        rh = data_[(x + 1) + z * width].position_.y_;
      }

      if (z < h_ - 1) {
        uh = data_[x + (z + 1) * width].position_.y_;
      }

      if (z > 0) {
        dh = data_[x + (z - 1) * width].position_.y_;
      }

      Vec3 a(2.0f / w_, rh - lh, 0.0f);
      Vec3 b(0.0, uh - dh, 2.0f / h_);

      data_[x + z * width].normal_ = Vec3::Normalize(Vec3::Cross(b, a));
    }
  }

  return data_;
}

float Water::GetEnergy(int x, int y) { return energy_[x + y * w_]; }

void Water::SetEnergy(int x, int y, float energy) { energy_[x + y * w_] = energy; }

Water &Water::operator=(const Water &water) = default;
