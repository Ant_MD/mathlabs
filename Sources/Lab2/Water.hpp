#ifndef MATH_LABS_LAB2_WATER_HPP
#define MATH_LABS_LAB2_WATER_HPP

#include "../Core/Primitives/Mesh.hpp"

#include <vector>

struct WaterParams {
  float h_ = 0.0f;
  float u_ = 0.0f;
  float speed_ = 0.0f;
};

class Water {
public:
  Water(int w, int h);

  Water &operator=(const Water &water);

  static Water FlatWater(int w, int h, float height);
  static Water HillWater(int w, int h, float r, float a, float height);

  float GetHeight(int x, int y);
  float TryGetHeight(int x, int y, float def);
  void SetHeight(int x, int y, float value);

  float GetSpeed(int x, int y);
  void SetSpeed(int x, int y, float value);

  float GetEnergy(int x, int y);
  void SetEnergy(int x, int y, float energy);

  std::vector<Core::Vertex> &GetData();

private:
  std::vector<Core::Vertex> data_;
  std::vector<float> height_;
  std::vector<float> speed_;
  std::vector<float> energy_;
  int w_;
  int h_;
};

#endif // MATH_LABS_LAB2_WATER_HPP
