#include "SimpleSimulation.hpp"

#include "../Core/Math/Math.hpp"

#include <cmath>

using namespace Core;
using namespace std;

SimpleSimulation::SimpleSimulation(int w, int h) : w_(w), h_(h), time_(0.0f), water_(w, h) {}

void SimpleSimulation::Update() {
  time_ += 0.02f;

  for (int y = 0; y < h_; y++) {
    for (int x = 0; x < w_; x++) {
      float r = Math::Length(w_ / 2.0f - x, h_ / 2.0f - y);
      float v = (cos(-time_ + r * 0.2f) + 1.0f) / 40.0f + 1.0f;

      water_.SetHeight(x, y, v);
    }
  }
}
