#ifndef MATH_LABS_LAB2_TINY_WATER_SIMULATION_HPP
#define MATH_LABS_LAB2_TINY_WATER_SIMULATION_HPP

#include "../Core/Primitives/Mesh.hpp"
#include "Water.hpp"
#include "WaterSimulation.hpp"

#include <vector>

class TinyWaterSimulation : public WaterSimulation {
public:
  TinyWaterSimulation(int w, int h, const Water &defaultWater);

  Water &GetWater() final { return water_; };

  void Update() final;

private:
  void WaveFunction(WaterParams &wp, int i, int j);

  int w_;
  int h_;

  float time_;
  float vp_;
  float step_;
  float deltatime_;

  Water water_;
  Water newWater_;

  std::vector<float> energy_;
  std::vector<float> speed_;
};

#endif // MATH_LABS_LAB2_TINY_WATER_SIMULATION_HPP
