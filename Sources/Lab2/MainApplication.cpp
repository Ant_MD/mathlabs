#include "MainApplication.hpp"

#include "../Core/Math/Math.hpp"
#include "../Core/Primitives/Uniforms/UniformVec3.hpp"
#include "FiniteDifferenceSimulation.hpp"
#include "RoungeKuttSimulation.hpp"
#include "SimpleSimulation.hpp"
#include "TinyWaterSimulation.hpp"

#include "GLFW/glfw3.h"

#include <chrono>
#include <cmath>
#include <iostream>

using namespace Core;
using namespace std;

MainApplication::MainApplication()
    : Application(), currentMethod_(9), keyPressed_(false), camera_(new Camera()), floor_(new Object),
      water_(new Object()), light_(new Object()), skybox_(new Object()),
      simulation_(new SimpleSimulation(WIDTH, HEIGHT)) {}

void MainApplication::OnRedraw() {
  ClearColor(0.098f, 0.098f, 0.439f);
  ClearDepth();

  Draw(light_.get(), camera_.get(), {}, false);

  Draw(floor_.get(), camera_.get(), {}, false);

  UniformVec3 camPos("uCameraPos", camera_->GetPosition());
  UniformVec3 lightDir("uLightDir", light_->GetPosition());
  Draw(water_.get(), camera_.get(), {&camPos, &lightDir}, false);

  Draw(skybox_.get(), camera_.get(), {}, false);
}

void MainApplication::OnStart() {
  camera_->SetProjection(60.0f, 4.0f / 3.0f, 0.5f, 80.0f);
  camera_->SetPosition({0.0f, 3.0f, -3.0f});

  SetEnvmap({"Binaries/Lab1/SkyRight.png", "Binaries/Lab1/SkyLeft.png", "Binaries/Lab1/SkyUp.png",
             "Binaries/Lab1/SkyDown.png", "Binaries/Lab1/SkyBack.png", "Binaries/Lab1/SkyFront.png"});

  GenerateWater();
  GenerateLight();
  GenerateFloor();
  GenerateSkybox();
}

void MainApplication::OnUpdate() {
  UpdateCamera();

  UpdateWater();
}

void MainApplication::GenerateWater() {
  string waterVsSource = LoadStringFromFile("Binaries/Lab1/Water.vs");
  string waterFsSource = LoadStringFromFile("Binaries/Lab1/Water.fs");

  auto *shader = new Shader();
  shader->Compile(waterVsSource, waterFsSource);
  std::cerr << shader->GetError();

  auto *texture = new Texture();
  texture->LoadFromFile("Binaries/Lab1/Floor.jpg");

  vector<unsigned int> indices;
  indices.reserve(WIDTH * HEIGHT * 6);

  for (int z = 0; z < HEIGHT - 1; z++) {
    for (int x = 0; x < WIDTH - 1; x++) {
      indices.push_back(static_cast<unsigned int &&>(x + z * WIDTH));
      indices.push_back(static_cast<unsigned int &&>(x + (z + 1) * WIDTH));
      indices.push_back(static_cast<unsigned int &&>((x + 1) + (z + 1) * WIDTH));
      indices.push_back(static_cast<unsigned int &&>(x + z * WIDTH));
      indices.push_back(static_cast<unsigned int &&>((x + 1) + (z + 1) * WIDTH));
      indices.push_back(static_cast<unsigned int &&>((x + 1) + z * WIDTH));
    }
  }

  auto *mesh = new Mesh();
  mesh->SetIndices(indices);
  mesh->SetData(simulation_->GetWater().GetData());

  water_->SetMesh(mesh);
  water_->SetTexture(texture);
  water_->SetShader(shader);
}

void MainApplication::GenerateLight() {
  vector<Vertex> vs = {{{0.0f, -1.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f}},
                       {{-0.5f, 0.0f, 0.5f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f}},
                       {{0.0f, 0.0f, -0.5f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f}},
                       {{0.5f, 0.0f, 0.5f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f}}};

  vector<unsigned int> indices = {1, 2, 3, 0, 2, 1, 0, 3, 2, 0, 1, 3};

  string lightVsSource = LoadStringFromFile("Binaries/Lab1/Light.vs");
  string lightFsSource = LoadStringFromFile("Binaries/Lab1/Light.fs");

  auto *lightShader_ = new Shader();
  lightShader_->Compile(lightVsSource, lightFsSource);

  std::cerr << lightShader_->GetError();

  auto *mesh = new Mesh();

  mesh->SetData(vs);
  mesh->SetIndices(indices);

  light_->SetMesh(mesh);
  light_->SetScale(0.05f);
  light_->SetShader(lightShader_);
  light_->SetPosition({6.f, 6.0f, 16.0f});
}

void MainApplication::GenerateFloor() {
  vector<Vertex> vs = {{{-1.0f, 0.0f, 1.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f}},
                       {{-1.0f, 0.0f, -1.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 1.0f}},
                       {{1.0f, 0.0f, -1.0f}, {0.0f, 0.0f, 0.0f}, {1.0f, 1.0f}},
                       {{1.0f, 0.0f, 1.0f}, {0.0f, 0.0f, 0.0f}, {1.0f, 0.0f}}};

  vector<unsigned int> indices = {0, 1, 2, 0, 2, 3};

  auto texture = new Texture();
  texture->LoadFromFile("Binaries/Lab1/Floor.jpg");

  string lightVsSource = LoadStringFromFile("Binaries/Lab1/Floor.vs");
  string lightFsSource = LoadStringFromFile("Binaries/Lab1/Floor.fs");

  auto *shader = new Shader();
  shader->Compile(lightVsSource, lightFsSource);

  std::cerr << shader->GetError();

  auto *mesh = new Mesh();
  mesh->SetData(vs);
  mesh->SetIndices(indices);

  floor_->SetMesh(mesh);
  floor_->SetShader(shader);
  floor_->SetPosition({0.0f, 0.0f, 0.0f});
  floor_->SetTexture(texture);
}

void MainApplication::UpdateWater() {
  if (KeyPressed(GLFW_KEY_1) || KeyPressed(GLFW_KEY_2) || KeyPressed(GLFW_KEY_3) || KeyPressed(GLFW_KEY_4)) {
    if (!keyPressed_ && KeyPressed(GLFW_KEY_1)) {
      simulation_ = std::make_shared<SimpleSimulation>(WIDTH, HEIGHT);
    } else if (!keyPressed_ && KeyPressed(GLFW_KEY_2)) {
      simulation_ = std::make_shared<FiniteDifferenceSimulation>(WIDTH, HEIGHT,
                                                                 Water::HillWater(WIDTH, HEIGHT, 10.0f, 0.2f, 1.0f));
    } else if (!keyPressed_ && KeyPressed(GLFW_KEY_3)) {
      simulation_ =
          std::make_shared<RoungeKuttSimulation>(WIDTH, HEIGHT, Water::HillWater(WIDTH, HEIGHT, 10.0f, 0.2f, 1.0f));
    } else if (!keyPressed_ && KeyPressed(GLFW_KEY_4)) {
      simulation_ =
          std::make_shared<TinyWaterSimulation>(WIDTH, HEIGHT, Water::HillWater(WIDTH, HEIGHT, 10.0f, 0.2f, 1.0f));
    }

    keyPressed_ = true;

  } else {
    keyPressed_ = false;
  }

  simulation_->Update();

  water_->GetMesh()->SetData(simulation_->GetWater().GetData());
}

void MainApplication::GenerateSkybox() {
  float size = 50.0f;
  vector<Vertex> vs = {{{-size, -size, -size}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f}},
                       {{size, -size, -size}, {0.0f, 0.0f, 0.0f}, {0.0f, 1.0f}},
                       {{size, -size, size}, {0.0f, 0.0f, 0.0f}, {1.0f, 1.0f}},
                       {{-size, -size, size}, {0.0f, 0.0f, 0.0f}, {1.0f, 0.0f}},
                       {{-size, size, -size}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f}},
                       {{size, size, -size}, {0.0f, 0.0f, 0.0f}, {0.0f, 1.0f}},
                       {{size, size, size}, {0.0f, 0.0f, 0.0f}, {1.0f, 1.0f}},
                       {{-size, size, size}, {0.0f, 0.0f, 0.0f}, {1.0f, 0.0f}}};

  vector<unsigned int> indices = {0, 1, 2, 0, 2, 3, 0, 4, 5, 0, 5, 1, 1, 5, 6, 1, 6, 2,
                                  3, 6, 2, 3, 7, 6, 0, 4, 7, 0, 7, 3, 4, 7, 6, 4, 6, 5};

  string lightVsSource = LoadStringFromFile("Binaries/Lab1/Skybox.vs");
  string lightFsSource = LoadStringFromFile("Binaries/Lab1/Skybox.fs");

  auto *shader = new Shader();
  shader->Compile(lightVsSource, lightFsSource);

  std::cerr << shader->GetError();

  auto *mesh = new Mesh();
  mesh->SetData(vs);
  mesh->SetIndices(indices);

  skybox_->SetMesh(mesh);
  skybox_->SetShader(shader);
  skybox_->SetPosition({0.0f, 0.0f, 0.0f});
}

void MainApplication::UpdateCamera() {
  Vec3 cameraPos = camera_->GetPosition();
  float yaw = camera_->GetYaw();
  float pitch = camera_->GetPitch();

  if (KeyPressed(GLFW_KEY_D)) {
    cameraPos.x_ += sin(Math::ToRad(yaw + 90)) * 0.05f;
    cameraPos.z_ -= cos(Math::ToRad(yaw + 90)) * 0.05f;
  }

  if (KeyPressed(GLFW_KEY_A)) {
    cameraPos.x_ -= sin(Math::ToRad(yaw + 90)) * 0.05f;
    cameraPos.z_ += cos(Math::ToRad(yaw + 90)) * 0.05f;
  }

  if (KeyPressed(GLFW_KEY_W)) {
    cameraPos.x_ += sin(Math::ToRad(yaw)) * 0.05f * cos(Math::ToRad(pitch));
    cameraPos.y_ -= sin(Math::ToRad(pitch)) * 0.05f;
    cameraPos.z_ -= cos(Math::ToRad(yaw)) * 0.05f * cos(Math::ToRad(pitch));
  }

  if (KeyPressed(GLFW_KEY_S)) {
    cameraPos.x_ -= sin(Math::ToRad(yaw)) * 0.05f * cos(Math::ToRad(pitch));
    cameraPos.y_ += sin(Math::ToRad(pitch)) * 0.05f;
    cameraPos.z_ += cos(Math::ToRad(yaw)) * 0.05f * cos(Math::ToRad(pitch));
  }

  pitch += cursorDelta_.y_;
  camera_->SetPitch(Math::Clamp(-90.f, 90.0f, pitch));

  yaw += cursorDelta_.x_;
  camera_->SetYaw(yaw);

  camera_->SetPosition(cameraPos);
  skybox_->SetPosition(cameraPos);
}

int main() {
  std::shared_ptr<Application> app = std::make_shared<MainApplication>();

  app->Start();
}
