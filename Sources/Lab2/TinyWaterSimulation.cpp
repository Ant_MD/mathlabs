#include "TinyWaterSimulation.hpp"

#include "../Core/Math/Math.hpp"

#include <cmath>
#include <iostream> //

using namespace Core;
using namespace std;

void TinyWaterSimulation::WaveFunction(WaterParams &v, int i, int j) {
  float g = 0.9;

  int r = (i + 1) % w_;
  int l = (i - 1 + w_) % w_;
  int u = (j + 1) % w_;
  int d = (j - 1 + w_) % w_;

  v.h_ = -((water_.GetEnergy(r, j) - water_.GetEnergy(i, j)) / step_) -
         ((water_.GetSpeed(i, u) - water_.GetSpeed(i, j)) / step_);

  v.u_ =
      -((((Math::Sqr(water_.GetEnergy(i, j)) / water_.GetHeight(i, j)) + (g * Math::Sqr(water_.GetHeight(i, j)) / 2)) -
         ((Math::Sqr(water_.GetEnergy(l, j)) / water_.GetHeight(l, j)) + (g * Math::Sqr(water_.GetHeight(l, j)) / 2))) /
        (step_)) -
      ((water_.GetEnergy(i, j) * water_.GetSpeed(i, j) / water_.GetHeight(i, j)) -
       (water_.GetEnergy(i, d) * water_.GetSpeed(i, d) / water_.GetHeight(i, d)) / step_);

  v.speed_ =
      -((((Math::Sqr(water_.GetSpeed(i, j)) / water_.GetHeight(i, j)) + (g * Math::Sqr(water_.GetHeight(i, j)) / 2)) -
         ((Math::Sqr(water_.GetSpeed(i, d)) / water_.GetHeight(i, d)) + (g * Math::Sqr(water_.GetHeight(i, d)) / 2))) /
        step_) -
      ((water_.GetEnergy(i, j) * water_.GetSpeed(i, j) / water_.GetHeight(i, j)) -
       (water_.GetEnergy(l, j) * water_.GetSpeed(l, j) / water_.GetHeight(l, j)) / step_);
}

TinyWaterSimulation::TinyWaterSimulation(int w, int h, const Water &defaultWater)
    : w_(w), h_(h), time_(0.0f), vp_(1.0f), step_(2.0f / (w_ - 1)), deltatime_(0.02f * (step_ / vp_)),
      water_(defaultWater), newWater_(defaultWater), energy_(w_ * h_), speed_(w_ * h_) {
  for (int z = 0; z < h_; z++) {
    for (int x = 0; x < w_; x++) {
      energy_[w_ * z + x] = 0.0f;
      speed_[w_ * z + x] = 0.0f;
    }
  }
}

void TinyWaterSimulation::Update() {
  Water waterBuffer = water_;
  Water k1(w_, h_);
  Water k2(w_, h_);
  Water k3(w_, h_);
  WaterParams buffer{};

  for (int i = 0; i < (w_ - 0); i++) {
    for (int j = 0; j < (h_ - 0); j++) {
      // buffer.h_ = water_.GetHeight(i, j);
      // buffer.speed_ = water_.GetSpeed(i, j);
      WaveFunction(buffer, i, j);
      k1.SetHeight(i, j, buffer.h_);
      k1.SetSpeed(i, j, buffer.speed_);
      k1.SetEnergy(i, j, buffer.u_);

      buffer.h_ = water_.GetHeight(i, j) + k1.GetHeight(i, j) * deltatime_;
      buffer.speed_ = water_.GetSpeed(i, j) + k1.GetSpeed(i, j) * deltatime_;
      buffer.u_ = water_.GetEnergy(i, j) + k1.GetEnergy(i, j) * deltatime_;

      waterBuffer.SetHeight(i, j, buffer.h_);
      waterBuffer.SetSpeed(i, j, buffer.speed_);
    }
  }

  for (int i = 0; i < (w_ - 0); i++) {
    for (int j = 0; j < (h_ - 0); j++) {
      // buffer.h_ = waterBuffer.GetHeight(i, j);
      // buffer.speed_ = waterBuffer.GetSpeed(i, j);
      WaveFunction(buffer, i, j);
      k2.SetHeight(i, j, buffer.h_);
      k2.SetSpeed(i, j, buffer.speed_);
      k2.SetEnergy(i, j, buffer.u_);

      buffer.h_ = water_.GetHeight(i, j) + k2.GetHeight(i, j) * deltatime_;
      buffer.speed_ = water_.GetSpeed(i, j) + k2.GetSpeed(i, j) * deltatime_;
      buffer.u_ = water_.GetEnergy(i, j) + k2.GetEnergy(i, j) * deltatime_;

      waterBuffer.SetHeight(i, j, buffer.h_);
      waterBuffer.SetSpeed(i, j, buffer.speed_);
    }
  }

  for (int i = 0; i < (w_ - 0); i++) {
    for (int j = 0; j < (h_ - 0); j++) {
      // buffer.h_ = waterBuffer.GetHeight(i, j);
      // buffer.speed_ = waterBuffer.GetSpeed(i, j);
      WaveFunction(buffer, i, j);
      k3.SetHeight(i, j, buffer.h_);
      k3.SetSpeed(i, j, buffer.speed_);
      k3.SetEnergy(i, j, buffer.u_);

      buffer.h_ = water_.GetHeight(i, j) + k3.GetHeight(i, j) * deltatime_ * 2;
      buffer.speed_ = water_.GetSpeed(i, j) + k3.GetSpeed(i, j) * deltatime_ * 2;
      buffer.u_ = water_.GetEnergy(i, j) + k3.GetEnergy(i, j) * deltatime_ * 2;

      waterBuffer.SetHeight(i, j, buffer.h_);
      waterBuffer.SetSpeed(i, j, buffer.speed_);
    }
  }

  for (int i = 0; i < (w_ - 0); i++) {
    for (int j = 0; j < (h_ - 0); j++) {
      // buffer.h_ = waterBuffer.GetHeight(i, j);
      // buffer.speed_ = waterBuffer.GetSpeed(i, j);
      WaveFunction(buffer, i, j);

      buffer.h_ =
          water_.GetHeight(i, j) +
          (deltatime_ / 3.0f *
           (k1.GetHeight(i, j) + deltatime_ * k2.GetHeight(i, j) + deltatime_ * k3.GetHeight(i, j) + buffer.h_));
      buffer.speed_ =
          water_.GetSpeed(i, j) +
          (deltatime_ / 3.0f *
           (k1.GetSpeed(i, j) + deltatime_ * k2.GetSpeed(i, j) + deltatime_ * k3.GetSpeed(i, j) + buffer.speed_));
      buffer.u_ =
          water_.GetEnergy(i, j) +
          (deltatime_ / 3.0f *
           (k1.GetEnergy(i, j) + deltatime_ * k2.GetEnergy(i, j) + deltatime_ * k3.GetEnergy(i, j) + buffer.u_));

      waterBuffer.SetHeight(i, j, buffer.h_);
      waterBuffer.SetSpeed(i, j, buffer.speed_);
      waterBuffer.SetEnergy(i, j, buffer.u_);
    }
  }

  water_ = waterBuffer;
}
