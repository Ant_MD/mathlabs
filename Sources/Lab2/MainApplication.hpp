#ifndef MATH_LABS_LAB2_MAIN_APPLICATION
#define MATH_LABS_LAB2_MAIN_APPLICATION

#include "../Core/Application.hpp"
#include "../Core/Camera.hpp"
#include "../Core/Object.hpp"
#include "../Core/Primitives/Mesh.hpp"
#include "../Core/Primitives/Shader.hpp"
#include "../Core/Primitives/Texture.hpp"
#include "WaterSimulation.hpp"

#include "memory"

class MainApplication : public Core::Application {
public:
  MainApplication();

protected:
  void OnStart() final;
  void OnRedraw() final;
  void OnUpdate() final;

private:
  void GenerateWater();
  void GenerateLight();
  void GenerateFloor();
  void GenerateSkybox();

  void UpdateWater();
  void UpdateCamera();

  const int WIDTH = 100;
  const int HEIGHT = 100;

  int currentMethod_;
  bool keyPressed_;

  std::shared_ptr<Core::Camera> camera_;

  std::shared_ptr<Core::Object> floor_;
  std::shared_ptr<Core::Object> water_;
  std::shared_ptr<Core::Object> light_;
  std::shared_ptr<Core::Object> skybox_;

  std::shared_ptr<WaterSimulation> simulation_;
};

#endif // MATH_LABS_LAB2_MAIN_APPLICATION
